//
//  GeneralHelper.swift
//  UTalk
//
//  Created by apple on 5/30/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class GeneralHelper: NSObject {
    
    //MARK:- Email Validation
    class func isValidEmail(email:String?) -> Bool {
        guard email != nil else { return false }
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
    
    
    
    //MARK:- Show Alert
    class func displayAlertMessage(messageToDisplay: String, sender: UIViewController)
    {
        let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        {
            (actIon: UIAlertAction!) in
            print("Ok Button tapped")
        }
        alertController.addAction(OKAction)
        sender.present(alertController, animated: true, completion: nil)
    }
}
