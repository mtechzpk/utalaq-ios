//
//  otpVC.swift
//  UTalk
//
//  Created by apple on 5/30/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class otpVC: BaseViewController, UITextFieldDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var otpTF1: UITextField!
    @IBOutlet weak var otpTF2: UITextField!
    @IBOutlet weak var otpTF3: UITextField!
    @IBOutlet weak var otpTF4: UITextField!
    @IBOutlet weak var resetBtn: UIButton!
    
    //MARK:- Variables
    
    //MARK:- Arrays
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        otpTF1.delegate = self
        otpTF2.delegate = self
        otpTF3.delegate = self
        otpTF4.delegate = self
        resetBtn.roundButton()
    }
    
    //MARK:- Actions
    @IBAction func dismissClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func SubmitCLicked(_ sender: UIButton) {
        if otpTF1.text == "" || otpTF2.text == "" || otpTF3.text == "" || otpTF4.text == "" {
            GeneralHelper.displayAlertMessage(messageToDisplay: "Add your valid code", sender: self)
        } else {
            self.showProgressHUD()
            let param: Parameters = ["code": "\(otpTF1.text!)\(otpTF2.text!)\(otpTF3.text!)\(otpTF4.text!)"]
            let url = "\(NetworkManager.BASE_URL)verify_code"
            Alamofire.request(url, method: .post, parameters: param).responseData { response in
                self.hideProgressHUD()
                switch response.result{
                case.success(let value):
                    let json = JSON(value)
                    print(json)
                    let status = json["status"].intValue
                    if status == 400{
                        GeneralHelper.displayAlertMessage(messageToDisplay: "Invalid code!", sender: self)
                    } else {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                        self.present(vc, animated: true, completion: nil)
                    }
                case.failure(let error):
                    print(error.localizedDescription)
                    GeneralHelper.displayAlertMessage(messageToDisplay: "\(error.localizedDescription)", sender: self)
                }
            }
        }
        
    }
    
    //MARK:- Functions
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ((textField.text?.count)! < 1) && (string.count > 0){
            if textField == otpTF1{
                otpTF2.becomeFirstResponder()
            }
            if textField == otpTF2{
                otpTF3.becomeFirstResponder()
            }
            if textField == otpTF3{
                otpTF4.becomeFirstResponder()
            }
            if textField == otpTF4{
                otpTF4.resignFirstResponder()
            }
            textField.text = string
            return false
        } else if ((textField.text?.count)! >= 1) && (string.count == 0){
            if textField == otpTF1{
                otpTF1.resignFirstResponder()
            }
            if textField == otpTF2{
                otpTF1.becomeFirstResponder()
            }
            if textField == otpTF3{
                otpTF2.becomeFirstResponder()
            }
            if textField == otpTF4{
                otpTF3.becomeFirstResponder()
            }
            textField.text = ""
            return false
        } else if (textField.text?.count)! >= 1{
            textField.text = string
            return false
        }
        return true
    }
    
}
