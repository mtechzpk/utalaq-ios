//
//  DashboardTopTVCell.swift
//  UTalk
//
//  Created by apple on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class DashboardTopTVCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
