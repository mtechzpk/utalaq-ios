//
//  DashboardVC.swift
//  UTalk
//
//  Created by apple on 5/7/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import FittedSheets
import Kingfisher
class DashboardVC: BaseViewController {

    //MARK:- Outlets
    @IBOutlet weak var tabbarView: UIView!
    @IBOutlet weak var topTableView: UITableView!
    @IBOutlet weak var typeTableView: UITableView!
    @IBOutlet weak var whatsOnYourMindTF: UITextField!
    @IBOutlet weak var searchTF: customizeImageTF!
    
    @IBOutlet weak var profilestatusLbl: UILabel!
    
    //MARK:- Arrays
    var arraysTypes:[String] = ["FAMILY (6)", "FRIENDS (272)", "CHATROOMS (3)", "FOLLOWINGS", "FOLLOWERS", "MESSAGE REQUEST"]
    var topfourusers = [User]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        whatsOnYourMindTF.attributedPlaceholder = NSAttributedString(string: "What's your mood today?", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        searchTF.attributedPlaceholder = NSAttributedString(string: "search..", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        getTopfourData()
        whatsOnYourMindTF.delegate = self
        profilestatusLbl.text = AppController.shared.loggedInUser?.profileStatus
    }
    
    //MARK:- Actions
    @IBAction func connectbtnClicked(_ sender: UIButton) {
        let controller = SheetViewController(controller: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConnectVC"), sizes: [.fullScreen, .fullScreen, .fixed(250)])
        self.present(controller, animated: false, completion: nil)
    }
    @IBAction func searchBtnClicked(_ sender: UIButton) {
    }
    
    
    
    
    //MARK:- Functions
    func getTopfourData() {
        self.showProgressHUD()
        NetworkManager.Fetchtopfour(){ (response) in
            self.hideProgressHUD()
            
            guard let response = response else { return }
            
            if response.status == 200, let users = User.tousersModel(response.data?.array) {
                
                self.topfourusers = users
                DispatchQueue.main.async {
                self.topTableView.reloadData()
                }
            }
        }
    }
}

extension DashboardVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == topTableView{
            return topfourusers.count
        } else {
            return arraysTypes.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == topTableView{
            let user = topfourusers[indexPath.row]
            let cell = topTableView.dequeueReusableCell(withIdentifier: "DashboardTopTVCell", for: indexPath) as! DashboardTopTVCell
            cell.nameLbl.text = "\(user.firstName ?? "") \(user.lastName ?? "")"
            if user.image != nil{
                cell.userImage.kf.setImage(with: URL(string: user.image ?? ""))
            }
            return cell
        } else {
            let cell = typeTableView.dequeueReusableCell(withIdentifier: "DashboardTypeTVCell", for: indexPath) as! DashboardTypeTVCell
            cell.typeLbl.text = arraysTypes[indexPath.row]
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == topTableView{
            return 55
        } else {
            return 50
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == topTableView{
            let controller = SheetViewController(controller: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileInfoVC"), sizes: [.halfScreen, .fullScreen, .fixed(250)])
            let childvc = controller.childViewController as! ProfileInfoVC
            childvc.userdata = topfourusers[indexPath.row]
            self.present(controller, animated: false, completion: nil)
            tableView.deselectRow(at: indexPath, animated: true)

        }
        else if tableView == typeTableView{
            if indexPath.row == 1{
                let controller = SheetViewController(controller: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FriendsVC"), sizes: [.fullScreen, .fullScreen, .fixed(250)])
                self.present(controller, animated: false, completion: nil)
                tableView.deselectRow(at: indexPath, animated: true)
            } else if indexPath.row == 0{
                let controller = SheetViewController(controller: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserOwnProfileVC"), sizes: [.fullScreen, .fullScreen, .fixed(250)])
                self.present(controller, animated: false, completion: nil)
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
}


extension DashboardVC:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        updateStatus(status: textField.text)
        print(textField.text)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func updateStatus(status:String?) {
        self.showProgressHUD()
        
        NetworkManager.updateProfileStatus(status: status) { (response) in
            self.hideProgressHUD()
            
            guard let response = response else { return }
            
            if response.status == 200{
                
                DispatchQueue.main.async {
                    self.showAlert(message: response.message ?? "Status Updated", title: "Success", action: UIAlertAction(title: "OK", style: .cancel, handler: { (alert) in
                        self.profilestatusLbl.text = status
                    }))
                }
            }
        }
    }
}
