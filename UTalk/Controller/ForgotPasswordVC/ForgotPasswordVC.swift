//
//  ForgotPasswordVC.swift
//  UTalk
//
//  Created by apple on 5/30/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ForgotPasswordVC: BaseViewController {

    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var resetBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        resetBtn.roundButton()
        emailTF.attributedPlaceholder = NSAttributedString(string: "Enter Email",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
    }
    @IBAction func dismissClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetBtnClicked(_ sender: UIButton) {
        if emailTF.text == ""{
            GeneralHelper.displayAlertMessage(messageToDisplay: "Enter your valid email address", sender: self)
        } else if !GeneralHelper.isValidEmail(email: emailTF.text){
            GeneralHelper.displayAlertMessage(messageToDisplay: "Email is not valid", sender: self)
        } else {
            self.showProgressHUD()
            let param:Parameters = ["email": emailTF.text!]
            let url = "\(NetworkManager.BASE_URL)forgot_password"
            let header = ["Accept":"application/json"]
            Alamofire.request(url, method: .post, parameters: param, headers: header).responseData { response in
                self.hideProgressHUD()
                switch response.result{
                case.success(let value):
                    let json = JSON(value)
                    print(json)
                    let status = json["status"].intValue
                    if status == 200{
                        GeneralHelper.displayAlertMessage(messageToDisplay: "The User with this Email Does Not Exist!", sender: self)
                    } else {
                        resetEmail = self.emailTF.text!
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "otpVC") as! otpVC
                        self.present(vc, animated: true, completion: nil)
                    }
                case.failure(let error):
                    print(error.localizedDescription)
                    GeneralHelper.displayAlertMessage(messageToDisplay: "\(error.localizedDescription)", sender: self)
                }
            }
        }
    }
    
}
