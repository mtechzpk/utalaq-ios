//
//  UserOwnProfileVC.swift
//  UTalk
//
//  Created by apple on 5/9/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UserOwnProfileVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var UserFriendsCollectionView: UICollectionView!
    @IBOutlet weak var UserImageCollectionView: UICollectionView!
    
    //MARK:- Variables
    
    //MARK:- Arrays
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension UserOwnProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == UserFriendsCollectionView{
            return 4
        } else {
            return 4
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == UserFriendsCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserTopFriendsCVCell", for: indexPath) as! UserTopFriendsCVCell
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserImagesCVCell", for: indexPath) as! UserImagesCVCell
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == UserFriendsCollectionView{
            return CGSize(width: 75, height: 100)
        } else {
            return CGSize(width: collectionView.frame.width - 100, height: collectionView.frame.height)
        }
    }
    
}
