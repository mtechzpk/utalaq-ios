//
//  UserImagesCVCell.swift
//  UTalk
//
//  Created by apple on 5/9/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UserImagesCVCell: UICollectionViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    
    override func awakeFromNib() {
        userImage.layer.cornerRadius = 16
    }
}
