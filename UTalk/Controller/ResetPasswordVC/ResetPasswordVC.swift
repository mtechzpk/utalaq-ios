//
//  ResetPasswordVC.swift
//  UTalk
//
//  Created by apple on 5/30/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ResetPasswordVC: BaseViewController {

    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confrimPasswordTF: UITextField!
    @IBOutlet weak var resetBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        resetBtn.roundButton()
        passwordTF.attributedPlaceholder = NSAttributedString(string: "New password",
                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        confrimPasswordTF.attributedPlaceholder = NSAttributedString(string: "Confirm Password",
                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
    }
    @IBAction func resetClicked(_ sender: UIButton) {
        if passwordTF.text == "" || confrimPasswordTF.text == ""{
            GeneralHelper.displayAlertMessage(messageToDisplay: "Kindly add your password", sender: self)
        } else if passwordTF.text == confrimPasswordTF.text {
            self.showProgressHUD()
            let param: Parameters = ["email": resetEmail, "password": passwordTF.text!, "confirm_password":confrimPasswordTF.text!]
            let url = "\(NetworkManager.BASE_URL)reset_password"
            let header = ["Accept":"application/json"]
            Alamofire.request(url, method: .post, parameters: param, headers: header).responseData { response in
                self.hideProgressHUD()
                switch response.result{
                case.success(let value):
                    let json = JSON(value)
                    let status = json["status"].intValue
                    if status == 200{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.present(vc, animated: true, completion: nil)
                    }
                    print(json)
                case.failure(let error):
                    print(error.localizedDescription)
                }
            }
        } else{
            GeneralHelper.displayAlertMessage(messageToDisplay: "Passwords are not matching", sender: self)
        }
    }
    
}
