//
//  ConnectVC.swift
//  UTalk
//
//  Created by apple on 5/30/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SwipeMenuViewController

class ConnectVC: UIViewController {

    @IBOutlet weak var SwipeView: SwipeMenuView!
    
    
    var options: SwipeMenuViewOptions!
    var names: [String] = ["Followers","Followings"]
    var ViewControllers:[UIViewController] = [UIViewController()]
    
    override func viewDidLoad() {
        options = .init()
        configMenu()
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        SwipeView.reloadData(options: options)
    }
    func configMenu(){
        SwipeView.dataSource = self
        ViewControllers.removeAll()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let FOLLOWERS = storyboard.instantiateViewController(withIdentifier: "FollowerVC")
        let FOLLOWINGS = storyboard.instantiateViewController(withIdentifier: "FollowingVC")
        ViewControllers = [FOLLOWERS,FOLLOWINGS,]
        options.tabView.style = .segmented
        //options.tabView.itemView.font = UIFont(name: "Roboto-Bold", size: 13)!
        options.tabView.addition = .underline
        options.tabView.additionView.backgroundColor = #colorLiteral(red: 0.007995928638, green: 0.8574128747, blue: 0.8022821546, alpha: 1)
        options.tabView.itemView.selectedTextColor = #colorLiteral(red: 0.01445262507, green: 0.8652017713, blue: 0.8142253757, alpha: 1)
    }
    
}


//MARK: — Swipe Delegate
extension ConnectVC:SwipeMenuViewDataSource{
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        return ViewControllers[index]
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return names[index]
    }
    
    func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return ViewControllers.count
    }
}

