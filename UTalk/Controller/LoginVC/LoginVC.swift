//
//  ViewController.swift
//  UTalk
//
//  Created by apple on 5/7/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class LoginVC: BaseViewController {
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var faceIDView: ShadowView!
    @IBOutlet weak var keyboardView: ShadowView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email address",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        loginBtn.roundButton()
        signupBtn.roundButton()
        emailView.roundView()
        passView.roundView()
        passwordTextField.text = "abcdefgh"
        emailTextField.text = "mahad@gmail.com"
    }
    
    @IBAction func onClickLoginButton(_ sender: UIButton) {
        guard let emailTF = emailTextField?.text, emailTF.isValidEmail() else {
            self.showAlert(message: "Invalid Email")
            emailTextField?.becomeFirstResponder()
            return
        }
        
        guard let passwordTF = passwordTextField?.text, !passwordTF.isEmpty else {
            self.showAlert(message: "Password can not be empty")
            passwordTextField?.becomeFirstResponder()
            return
        }
        
        hitLoginApi(email: emailTF, password: passwordTF)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserDidLoginNotification"), object: nil, userInfo: ["userId": "3"])
//        DispatchQueue.main.async {
//            self.present(DashboardVC.instantiate(fromAppStoryboard: .Main), animated: true, completion: nil)
//        }
    }
    @IBAction func faceIDClicked(_ sender: UIButton) {
        faceIDView.backgroundColor = #colorLiteral(red: 1, green: 0.4575266242, blue: 0.239685595, alpha: 1)
        keyboardView.backgroundColor = .clear
    }
    @IBAction func KeyboardClicked(_ sender: UIButton) {
        keyboardView.backgroundColor = #colorLiteral(red: 1, green: 0.4575266242, blue: 0.239685595, alpha: 1)
        faceIDView.backgroundColor = .clear
    }
    @IBAction func forgetPasswordClicked(_ sender: UIButton) {
        self.present(ForgotPasswordVC.instantiate(fromAppStoryboard: .Main), animated: true, completion: nil)
    }
    
    
    @IBAction func onClickSignUpButton(_ sender: UIButton) {
        self.present(SignupVC.instantiate(fromAppStoryboard: .Main), animated: true, completion: nil)
    }
}

extension LoginVC {
    private func hitLoginApi(email: String?, password: String?) {
        self.showProgressHUD()
        NetworkManager.login(email: email, password: password) { (response) in
            self.hideProgressHUD()
            
            guard let response = response else { return }
            
            if response.status == 200, let user = User.toModel(response.data?.dictionary) {
                
                AppController.shared.loggedInUser = user
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserDidLoginNotification"), object: nil, userInfo: ["userId": "\(user.id!)"])
                DispatchQueue.main.async {
                    self.present(DashboardVC.instantiate(fromAppStoryboard: .Main), animated: true, completion: nil)
                }
            }
        }
    }
}

extension UIButton{
    func roundButton() {
        layer.cornerRadius = 10
    }
    func styleButton(color:UIColor) {
        layer.cornerRadius = 10
        layer.borderColor = color.cgColor
        layer.borderWidth = 2
    }
}

extension UIView{
    func roundView() {
        layer.cornerRadius = 10
    }
}
