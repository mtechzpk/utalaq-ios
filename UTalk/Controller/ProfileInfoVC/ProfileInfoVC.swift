//
//  ProfileInfoVC.swift
//  UTalk
//
//  Created by apple on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ProfileInfoVC: BaseViewController {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    var userdata:User?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //friendsSearchTF.attributedPlaceholder = NSAttributedString(string: "search..", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        nameLbl.text = "\(userdata?.firstName ?? "") \(userdata?.lastName ?? "")"
        statusLbl.text = userdata?.profileStatus
    }
    
    @IBAction func CallBtn_Click(_ sender: Any) {
        if client().isStarted() {
            
            
            weak var call: SINCall? = client().call().callUserVideo(withId: "\(userdata?.id!)")
            performSegue(withIdentifier: "callView", sender: call)
        }
    }
    
    @IBAction func followBtn_Click(_ sender: Any) {
        followUser()
    }
    
    func followUser() {
        self.showProgressHUD()
        
        NetworkManager.followUser(followto: userdata?.id) { (response) in
            self.hideProgressHUD()
            
            guard let response = response else { return }
            
            if response.status == 200{
                
                DispatchQueue.main.async {
                    self.showAlert(message: response.message ?? "Status Updated", title: "Success", action: UIAlertAction(title: "OK", style: .cancel, handler: { (alert) in
                    }))
                }
            }
        }
    }
    
    func unfollowUser() {
        self.showProgressHUD()
        
        NetworkManager.unfollowUser(followto: userdata?.id) { (response) in
            self.hideProgressHUD()
            
            guard let response = response else { return }
            
            if response.status == 200{
                
                DispatchQueue.main.async {
                    self.showAlert(message: response.message ?? "Status Updated", title: "Success", action: UIAlertAction(title: "OK", style: .cancel, handler: { (alert) in
                    }))
                }
            }
        }
    }
    
    
    
    // MARK: - Functions
    func client() -> SINClient {
        return Global.client
    }
    // MARK: - Navigation
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           let callViewController = segue.destination as? CallViewController
           callViewController?.call = sender as? SINCall
       }
}
extension ProfileInfoVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileInfoCVCell", for: indexPath) as! ProfileInfoCVCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 75, height: 100)
    }
    
}
extension ProfileInfoVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileInfoTVCell", for: indexPath) as! ProfileInfoTVCell
        return cell
    }
}
