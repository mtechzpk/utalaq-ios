//
//  SignupVC.swift
//  UTalk
//
//  Created by apple on 5/7/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class SignupVC: BaseViewController {
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var fnameTF: UITextField!
    
    @IBOutlet weak var lnameTF: UITextField!
    
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var loginBtn: UIButton!
    private var imageSelectionAlertViewController: ImageSelectionAlertViewController?
    
    @IBOutlet weak var signupBtn: UIButton!
    private var selectedImage: UIImage? {
        didSet {
            DispatchQueue.main.async { self.userImageView.image = self.selectedImage ?? #imageLiteral(resourceName: "pic")}
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fnameTF.attributedPlaceholder = NSAttributedString(string: "First name",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        lnameTF.attributedPlaceholder = NSAttributedString(string: "Last name",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        emailTF.attributedPlaceholder = NSAttributedString(string: "Email address",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        passwordTF.attributedPlaceholder = NSAttributedString(string: "Password",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        signupBtn.roundButton()
        loginBtn.styleButton(color: .gray)
    }
    

    @IBAction func onClickSignUpButton(_ sender: UIButton) {

        
        guard let fname = fnameTF?.text, !fname.isEmpty else {
            self.showAlert(message: "please enter username")
            fnameTF?.becomeFirstResponder()
            return
        }
        
        guard let lastnameTF = lnameTF?.text, !lastnameTF.isEmpty else {
            self.showAlert(message: "Invalid Email")
            lnameTF?.becomeFirstResponder()
            return
        }
        
        guard let email = emailTF?.text, !email.isEmpty else {
            self.showAlert(message: "Password can not be empty")
            emailTF?.becomeFirstResponder()
            return
        }
        
        guard let password = passwordTF?.text, !password.isEmpty else {
            self.showAlert(message: "Password can not be empty")
            passwordTF?.becomeFirstResponder()
            return
        }
        
//        if self.selectedImage == nil {
//            self.showAlert(message: "Please select an image first")
//            return
//        }
        hitSignUpApi(fName: fname, lName: lastnameTF, email: email, password: password, image: self.selectedImage)
    }
    
    @IBAction func onClickSelectImageButton(_ sender: UIButton) {
        if imageSelectionAlertViewController == nil {
            imageSelectionAlertViewController = ImageSelectionAlertViewController(sender: sender, viewController: self, isEditable: true)
        }
        
        imageSelectionAlertViewController?.onImageSelected = { self.selectedImage = $0 }
        
        imageSelectionAlertViewController?.present()
    }
    
    @IBAction func onClickLoginButton(_ sender: UIButton) {
        self.present(LoginVC.instantiate(fromAppStoryboard: .Main), animated: true, completion: nil)
    }
}


// Api Calls
extension SignupVC {
    private func hitSignUpApi(fName: String?, lName: String? ,email: String?, password: String?, image: UIImage?) {
        self.showProgressHUD()
        NetworkManager.signUp(fName: fName, lName: lName, email: email, password: password, image: image) { (response) in
            self.hideProgressHUD()
            guard let response = response else { return }
            
            if response.status == 200, let user = User.toModel(response.data?.dictionary) {
                
                AppController.shared.loggedInUser = user
                
                DispatchQueue.main.async {
                    self.showAlert(message: "Signup Successfull", title: "Registration", action: UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        self.present(LoginVC.instantiate(fromAppStoryboard: .Main), animated: true, completion: nil)
                    }))
                }
            }
        }
    }
}
