//
//  BaseViewController.swift
//  UTalk
//
//  Created by Nabeel on 21/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    static let textViewPlaceholderText = "Tell the Influencers about the Campaign and what you expect from them"
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    private var progressHUD: ProgressHUD?
    
    var isProgressing: Bool {
        return self.progressHUD != nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let navController = self.navigationController {
            navController.navigationBar.barTintColor = UIColor.white
        }
    }
    
    func showAlert(message: String, title: String? = nil, action: UIAlertAction? = nil, secondAction: UIAlertAction? = nil) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            let alertController = UIAlertController(title: title,
                                                    message: message,
                                                    preferredStyle: .alert)
            
            alertController.addAction(action ?? UIAlertAction(title: "OK", style: .default, handler: nil))
            
            if let secondAction = secondAction {
                alertController.addAction(secondAction)
            }
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showProgressHUD(text: String = "Loading") {
        DispatchQueue.main.async {
            if let progressHUD = self.progressHUD {
                progressHUD.removeFromSuperview()
                self.progressHUD = nil
            }
            
            self.view.isUserInteractionEnabled = false
            self.progressHUD = ProgressHUD(text: text)
            self.view.addSubview(self.progressHUD!)
        }
    }
    
    func hideProgressHUD() {
        DispatchQueue.main.async {
            guard let progressHUD = self.progressHUD else { return }
            
            self.view.isUserInteractionEnabled = true
            progressHUD.removeFromSuperview()
            self.progressHUD = nil
        }
    }
}
