//
//  FollowingVC.swift
//  UTalk
//
//  Created by apple on 5/30/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class FollowingVC: UIViewController {

    @IBOutlet weak var topSearchTF: customizeImageTF!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        topSearchTF.attributedPlaceholder = NSAttributedString(string: "Search..", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
}
extension FollowingVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowingTVCell", for: indexPath) as! FollowingTVCell
        return cell
    }
}
