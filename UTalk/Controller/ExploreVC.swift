//
//  ExploreVC.swift
//  UTalk
//
//  Created by apple on 5/30/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SwipeMenuViewController

class ExploreVC: UIViewController {

    @IBOutlet weak var SwipeView: SwipeMenuView!
    
    
    var options: SwipeMenuViewOptions!
    var names: [String] = ["Users","Chatrooms"]
    var ViewControllers:[UIViewController] = [UIViewController()]
    
    override func viewDidLoad() {
        options = .init()
        configMenu()
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        SwipeView.reloadData(options: options)
    }
    func configMenu(){
        SwipeView.dataSource = self
        ViewControllers.removeAll()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let USER = storyboard.instantiateViewController(withIdentifier: "ExploreUserVC")
        let CHATROOMS = storyboard.instantiateViewController(withIdentifier: "ExploreChatroomsVC")
        ViewControllers = [USER,CHATROOMS,]
        options.tabView.style = .segmented
        //options.tabView.itemView.font = UIFont(name: "Roboto-Bold", size: 13)!
        options.tabView.addition = .underline
        options.tabView.additionView.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        options.tabView.itemView.selectedTextColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    
}


//MARK: — Swipe Delegate
extension ExploreVC:SwipeMenuViewDataSource{
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        return ViewControllers[index]
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return names[index]
    }
    
    func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return ViewControllers.count
    }
}
