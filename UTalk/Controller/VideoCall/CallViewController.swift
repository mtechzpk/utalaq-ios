//
//  CallViewController.swift
//  SinchVideoSwift
//
//  Created by Hassan on 18/10/2017.
//  Copyright © 2017 creatrixe. All rights reserved.
//

import UIKit
import AVFoundation

class CallViewController: UIViewController, SINCallDelegate {

//
     // MARK: - Outlets
    @IBOutlet var remoteVideoFullscreenGestureRecognizer: UITapGestureRecognizer!
    
    @IBOutlet var localVideoFullscreenGestureRecognizer: UITapGestureRecognizer!
    
    @IBOutlet var switchCameraGestureRecognizer: UITapGestureRecognizer!
    
//
    @IBOutlet var remoteUsername: UILabel!
    @IBOutlet var callStateLabel: UILabel!
    @IBOutlet var answerButton: UIButton!
    @IBOutlet var declineButton: UIButton!
    @IBOutlet var endCallButton: UIButton!
    @IBOutlet var remoteVideoView: UIView!
    @IBOutlet var localVideoView: UIView!
    
    
//
    // MARK: - Variables
    var durationTimer: Timer?
    weak var call: SINCall!
    private var audioPlayer: AVAudioPlayer?
//
//    
    enum EButtonsBar : Int {
        case kButtonsAnswerDecline
        case kButtonsHangup
    }
//
//    
//
     // MARK: - Load
    override func viewDidLoad() {
        super.viewDidLoad()

        setCall(self.call!)
        if call?.direction == SINCallDirection.incoming {
            self.callStateLabel.text = ""
            self.showButtons(EButtonsBar.kButtonsAnswerDecline)
            
            let alertSound = URL(fileURLWithPath: Bundle.main.path(forResource: "incoming", ofType: "wav")!)
            print(alertSound)
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
                try AVAudioSession.sharedInstance().setActive(true)
            } catch {
                print(error)
            }
            
            try! audioPlayer = AVAudioPlayer(contentsOf: alertSound)
            audioPlayer!.prepareToPlay()
            audioPlayer!.play()
            
        } else {
            self.callStateLabel.text = "calling..."
            self.showButtons(EButtonsBar.kButtonsHangup)
        }
        if  (call?.details.isVideoOffered)! {
            localVideoView.addSubview(videoController().localView())
            //remoteVideoView.addSubview(videoController().remoteView())
            localVideoFullscreenGestureRecognizer.require(toFail: switchCameraGestureRecognizer)
            videoController().localView().addGestureRecognizer(localVideoFullscreenGestureRecognizer)
            videoController().remoteView().addGestureRecognizer(remoteVideoFullscreenGestureRecognizer)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        remoteUsername.text = call?.remoteUserId
        remoteUsername.text = "Mahad"
    }
    
     // MARK: - Actions
    @IBAction func accept(sender: AnyObject) {
        audioPlayer?.stop()
        call?.answer()

    }
    
    @IBAction func decline(sender: AnyObject) {
        call?.hangup()
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func hangup(sender: AnyObject) {
        call?.hangup()
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func onSwitchCameraTapped(sender: AnyObject) {
        let current: AVCaptureDevice.Position = videoController().captureDevicePosition
        videoController().captureDevicePosition = SINToggleCaptureDevicePosition(current)

    }
    
    
    @IBAction func onFullScreenTapped(sender: AnyObject) {
        let view: UIView? = sender.view
        if view?.sin_isFullscreen() != nil {
            view?.contentMode = .scaleAspectFit
            view?.sin_disableFullscreen(true)
        }
        else {
            view?.contentMode = .scaleAspectFill
            view?.sin_enableFullscreen(true)
        }
    }
    
     // MARK: - Methods
    func setDuration(_ seconds: Int) {
        callStateLabel.text = String(format: "%02d:%02d", Int(seconds / 60), Int(seconds % 60))
    }
    
    @objc func onDurationTimer(_ unused: Timer) {
        let duration = Int(Date().timeIntervalSince((call?.details.establishedTime)!))
        self.setDuration(duration)
    }
    
    func audioController() -> SINAudioController {
        
        return Global.client.audioController()
    }
    
    func videoController() -> SINVideoController {
        
        return Global.client.videoController()
    }
    
    func setCall(_ call: SINCall) {
        self.call = call
        self.call?.delegate = self
    }
    
    func showButtons(_ buttons: EButtonsBar) {
        if buttons == EButtonsBar.kButtonsAnswerDecline {
            answerButton.isHidden = false
            declineButton.isHidden = false
            endCallButton.isHidden = true
        }
        else if buttons == EButtonsBar.kButtonsHangup {
            endCallButton.isHidden = false
            answerButton.isHidden = true
            declineButton.isHidden = true
        }
        
    }
    
    func startCallDurationTimer(with sel: Selector) {
        let selectorAsString: String = NSStringFromSelector(sel)
        durationTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.internal_updateDuration), userInfo: selectorAsString, repeats: true)
    }
    
    @objc func internal_updateDuration(_ timer: Timer) {
        let selector: Selector = NSSelectorFromString(timer.userInfo as! String)
        if responds(to: selector) {
            //clang diagnostic push
            //clang diagnostic ignored "-Warc-performSelector-leaks"
            perform(selector, with: timer)
            //clang diagnostic pop
        }
    }
    
    
    func stopCallDurationTimer() {
        durationTimer?.invalidate()
        durationTimer = nil
    }
    
     // MARK: - SINCall Delegates
    func callDidProgress(_ call: SINCall) {
        callStateLabel.text = "ringing..."
        let alertSound = URL(fileURLWithPath: Bundle.main.path(forResource: "ringback", ofType: "wav")!)
        print(alertSound)
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }

        try! audioPlayer = AVAudioPlayer(contentsOf: alertSound)
        audioPlayer!.prepareToPlay()
        audioPlayer!.play()
    }
    
    func callDidEstablish(_ call: SINCall) {
        startCallDurationTimer(with: #selector(self.onDurationTimer))
        showButtons(EButtonsBar.kButtonsHangup)
        audioController().stopPlayingSoundFile()
        localVideoView.isHidden = false
        remoteVideoView.isHidden = false
        remoteVideoView.addSubview(videoController().remoteView())
        
        
    }
    
    func callDidEnd(_ call: SINCall?) {
        dismiss(animated: true)
        audioController().stopPlayingSoundFile()
        stopCallDurationTimer()
        videoController().remoteView().removeFromSuperview()
        audioController().disableSpeaker()
    }
    func callDidAddVideoTrack(_ call: SINCall?) {
        
        remoteVideoView.addSubview(videoController().remoteView())
    }
    
    // MARK: - Sounds
    func path(forSound soundName: String) -> String {
        return URL(fileURLWithPath: (Bundle.main.resourcePath)!).appendingPathComponent(soundName).absoluteString
    }

}
