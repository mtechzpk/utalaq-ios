//
//  ChatVC.swift
//  UTalk
//
//  Created by apple on 5/28/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var messageTF: UITextField!
    
    //MARK:- Variables
    
    //MARK:- Arrays
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        messageTF.attributedPlaceholder = NSAttributedString(string: "Ask me anything", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
    }
    
    //MARK:- Actions
    
    //MARK:- Functions
    

}
extension ChatVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell?
        if indexPath.row % 2 == 0{
            cell = tableView.dequeueReusableCell(withIdentifier: "ReceiveMessagesTVCell", for: indexPath) as! ReceiveMessagesTVCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "SendMessagesTVCell", for: indexPath) as! SendMessagesTVCell
        }
        return cell!
    }
}
