//
//  FriendsVC.swift
//  UTalk
//
//  Created by apple on 5/7/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import FittedSheets
class FriendsVC: UIViewController {

    
    @IBOutlet weak var topSearchTF: UITextField!
    @IBOutlet weak var friendsSearchTF: customizeImageTF!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        topSearchTF.attributedPlaceholder = NSAttributedString(string: "Search for..", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        friendsSearchTF.attributedPlaceholder = NSAttributedString(string: "search..", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
    }
}

extension FriendsVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsTVCell", for: indexPath) as! FriendsTVCell
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = SheetViewController(controller: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileInfoVC"), sizes: [.halfScreen, .fullScreen, .fixed(250)])
        self.present(controller, animated: false, completion: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension UIView{
    func roundCorners(view :UIView, corners: UIRectCorner, radius: CGFloat){
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
    }
}
