//
//  AppDelegate.swift
//  UTalk
//
//  Created by apple on 5/7/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,SINClientDelegate {

    var window: UIWindow?
    var client: SINClient!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "UserDidLoginNotification"), object: nil, queue: nil, using: {(_ note: Notification) -> Void in
            self.initSinchClientwithUserId(userId: note.userInfo!["userId"] as! String)
        })
        
        FirebaseApp.configure()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func initSinchClientwithUserId(userId: String) {
        if (!(client != nil))
        {
            DispatchQueue.main.async {
                self.client = Sinch.client(withApplicationKey: "f6163e57-c2b3-43b6-8eb3-2f553be783c8", applicationSecret: "HPHWaQXf1k2hsXGHLf6H8A==", environmentHost: "clientapi.sinch.com", userId: userId)
                print (self.client!)
                self.client?.delegate = self
                self.client?.setSupportCalling(true)
                self.client?.setSupportMessaging(true)
                self.client?.startListeningOnActiveConnection()
                self.client?.start()
                Global.client = self.client
            }
        }
    }
    //MARK: - Delegates
    
    func client(_ client: SINClient, logMessage message: String, area: String, severity: SINLogSeverity, timestamp: Date) {
        print("\(message)")
    }
    func managedPush(_ managedPush: SINManagedPush!, didReceiveIncomingPushWithPayload payload: [AnyHashable : Any]!, forType pushType: String!) {
        
    }
    
    @objc func registerUserNotificationSettings() {
    }
    
    func clientDidStart(_ client: SINClient!) {
        print("Sinch client started successfully (version: \(String(describing: Sinch.version())))")
        
    }
    
    func clientDidFail(_ client: SINClient!, error: Error!) {
        print("Sinch client error: \(error.localizedDescription)")
    }

}

