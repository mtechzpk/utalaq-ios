//
//  MyUserDefaults.swift
//  UTalk
//
//  Created by Nabeel on 21/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class MyUserDefaults: NSObject {
    
    class func getUser() -> User? {
        let decoder = JSONDecoder()
        if let data = UserDefaults.standard.data(forKey: UserDefaults.Keys.user) {
            return try? decoder.decode(User.self, from: data)
        }
        return nil
    }
    
    class func setUser(_ user: User?) {
        if let user = user {
            let encoder = JSONEncoder()
            do {
                let encodedData = try encoder.encode(user)
                UserDefaults.standard.set(encodedData, forKey: UserDefaults.Keys.user)
            } catch {
                print("error: ", error)
            }
        } else {
            UserDefaults.standard.set(nil, forKey: UserDefaults.Keys.user)
        }
        UserDefaults.standard.synchronize()
    }
}

extension UserDefaults {
    enum Keys {
        static let preLoginIntro = "preLoginIntro"
        static let user = "key_user"
        static let fcmToken = "fcm_token"
        static let remoteConfigValues = "remote_config_values"
        static let instgramSkip = "instagram_skip"
        static let fcmData = "fcm_data"
        static let greetingMessage = "greeting_message"
        static let workSubmissionTutorial = "work_submission_tutorial"
        static let bannerMessage = "banner_message"
        static let influencerGreeting = "influencer_greeting_message"
    }
}
