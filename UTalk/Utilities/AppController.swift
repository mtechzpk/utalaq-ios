//
//  AppController.swift
//  amplifyd
//
//  Created by Noor Ali on 5/24/19.
//  Copyright © 2019 Logicon. All rights reserved.
//

import Foundation

class AppController {
    
    static let shared = AppController()
    
    private init() {}
    
    // if user is saved in our memory then get it from there else try to get user from storage
    private var _loggedInUser: User?
    var loggedInUser: User? {
        get {
            if _loggedInUser == nil {
                _loggedInUser = MyUserDefaults.getUser()
            }
            return _loggedInUser
        }
        set(newLoggedInUser) {
            _loggedInUser = newLoggedInUser
            MyUserDefaults.setUser(newLoggedInUser)
        }
    }
}

