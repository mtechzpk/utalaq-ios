//
//  String+Extension.swift
//  UTalk
//
//  Created by Nabeel on 21/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

extension String {
    
    var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
            // it is a link, if the match covers the whole string
            return match.range.length == self.utf16.count
        } else {
            return false
        }
    }
    
    func isValidEmail() -> Bool {
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: self)
    }
    
    func validatePassord(password: String) -> (Bool, Bool, Bool, Bool) {
        
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let isCapExists = texttest.evaluate(with: password)
        
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let isNumberExist = texttest1.evaluate(with: password)
        
        let specialCharacterRegEx  = ".*[!&^%$#@()/_*+-]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        let isSpecialCharacterExist = texttest2.evaluate(with: password)
        
        let smallLetterRegEx  = ".*[a-z].*"
        let texttest3 = NSPredicate(format:"SELF MATCHES %@", smallLetterRegEx)
        let isSmallExists = texttest3.evaluate(with: password)
        
        return (isCapExists, isNumberExist, isSpecialCharacterExist, isSmallExists)
    }
    
    
    
    func isValidPassword() -> Bool {
        let passwordRegex =  "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*[@#$%^&*+=!?-])(?=.*?[0-9]).{8,20}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        let indexRange = Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex))
        return String(self[indexRange])
    }
    
    private static var commaFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter
    }()
    
    internal var commaRepresentation: String? {
        guard let doubleValue = Double(self) else { return nil }
        return String.commaFormatter.string(from: NSNumber(value: doubleValue))
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}
