//
//  HTTPMethod.swift
//  amplifyd
//
//  Created by Noor Ali on 5/29/19.
//  Copyright © 2019 Logicon. All rights reserved.
//

import Foundation

public enum HTTPMethod : String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}
