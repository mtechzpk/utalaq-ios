//
//  NetworkManager.swift
//  UTalk
//
//  Created by Nabeel on 21/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

class NetworkManager {
    

    static let BASE_URL = "http://mtecsoft.com/utalq/public/api/"
    
    private static let timeOutIntervalForRequest = 120.0
    private static let timeOutIntervalForResource = 120.0
    
    fileprivate enum CachePolicy : String {
        case networkOnly           = "Network Only"
        case cacheOnly             = "Cache Only"
        case cacheElseNetwork      = "Cache Else Network"
        case networkElseCache      = "Network Else Cache"
        case reloadRevalidateCache = "Reload Revalidate Cache Data"
    }
    
    fileprivate enum HTTPMethod: String {
        case get     = "GET"
        case post    = "POST"
        case put     = "PUT"
        case patch   = "PATCH"
        case delete  = "DELETE"
    }
    
    fileprivate enum Result<String>{
        case success
        case failure(String)
    }
    
    fileprivate class func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
        case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
        case 600: return .failure(NetworkResponse.outdated.rawValue)
        default: return .failure(NetworkResponse.authenticationError.rawValue)
        }
    }
    
    
    fileprivate class func getSession(timeOutInterval: Double = timeOutIntervalForRequest) -> URLSession {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = timeOutInterval
        sessionConfig.timeoutIntervalForResource = timeOutInterval
        let session = URLSession(configuration: sessionConfig)
        return session
    }
    
    
    fileprivate class func processResponse(data: Data?, response: URLResponse?, responseError: Error?, completion: @escaping (ApiResponse?) -> ()) {
        DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
        
        guard responseError == nil else {
            completion(ApiResponse(message: responseError?.localizedDescription))
            return
        }
        
        guard let responseData = data else {
            completion(ApiResponse(message: NetworkResponse.noData.rawValue))
            return
        }
        
        guard let json = (try? JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
            print("Not containing JSON")
            return
        }
        
        completion(ApiResponse(json))
    }
    
    
    
    //MARK:- Network Calls
    // Netwrok Call with URL Sessions
    fileprivate class func fetchGenericData(urlString: String, paramsBag: NSDictionary? = nil, cachePolicy: CachePolicy = CachePolicy.networkOnly, completion: @escaping (ApiResponse?) -> ()) {
        if Connectivity.isConnectedToInternet {
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            guard let url = URL(string: urlString) else { return }
            
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            var headers = request.allHTTPHeaderFields ?? [:]
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = AppController.shared.loggedInUser?.token
            request.allHTTPHeaderFields = headers
            
            if let paramsBag = paramsBag, paramsBag.count > 0 {
                // will be JSON encoded
                let data = try! JSONSerialization.data(withJSONObject: paramsBag, options: JSONSerialization.WritingOptions.prettyPrinted)
                let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
            }
            
            // Network Only
            if cachePolicy == CachePolicy.networkOnly {
                NetworkManager.getDataFromNetwork(urlRequest: request, completion: completion)
            } else if cachePolicy == CachePolicy.cacheOnly { // Cache Only
                NetworkManager.getDataFromCache(urlRequest: request, completion: completion)
            } else if cachePolicy == CachePolicy.cacheElseNetwork { // Cache Else Network
                NetworkManager.getDataFromCacheElseNetwork(urlRequest: request, completion: completion)
            } else if cachePolicy == CachePolicy.networkElseCache { // Network Else Cache
                NetworkManager.getDataFromNetworkElseCache(urlRequest: request, completion: completion)
            } else if cachePolicy == CachePolicy.reloadRevalidateCache { // Revalidate Cache Data
                NetworkManager.revalidateCacheData(urlRequest: request, completion: completion)
            }
        } else {
            completion(ApiResponse(message: NetworkResponse.noInternet.rawValue))
        }
    }
    
    fileprivate class func fetchGenericDataForNotifications(urlString: String, paramsBag: NSDictionary? = nil, cachePolicy: CachePolicy = CachePolicy.networkOnly, completion: @escaping (ApiResponse?) -> ()) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        guard let url = URL(string: urlString) else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = AppController.shared.loggedInUser?.token
        request.allHTTPHeaderFields = headers
        
        if let paramsBag = paramsBag, paramsBag.count > 0 {
            // will be JSON encoded
            let data = try! JSONSerialization.data(withJSONObject: paramsBag, options: JSONSerialization.WritingOptions.prettyPrinted)
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
        
        // Network Only
        if cachePolicy == CachePolicy.networkOnly {
            NetworkManager.getDataFromNetwork(urlRequest: request, completion: completion)
        } else if cachePolicy == CachePolicy.cacheOnly { // Cache Only
            NetworkManager.getDataFromCache(urlRequest: request, completion: completion)
        } else if cachePolicy == CachePolicy.cacheElseNetwork { // Cache Else Network
            NetworkManager.getDataFromCacheElseNetwork(urlRequest: request, completion: completion)
        } else if cachePolicy == CachePolicy.networkElseCache { // Network Else Cache
            NetworkManager.getDataFromNetworkElseCache(urlRequest: request, completion: completion)
        } else if cachePolicy == CachePolicy.reloadRevalidateCache { // Revalidate Cache Data
            NetworkManager.revalidateCacheData(urlRequest: request, completion: completion)
        }
    }
    
    fileprivate class func postGenericRequest(urlString: String, paramsBag: NSDictionary, completion: @escaping (ApiResponse?) -> ()) {
        postGenericRequest(urlString: urlString, token: AppController.shared.loggedInUser?.token, paramsBag: paramsBag, completion: completion)
    }
    
    static func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    
    fileprivate class func postGenericRequest(urlString: String, token: String?, paramsBag: NSDictionary, completion: @escaping (ApiResponse?) -> ()) {
        DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = true }
        
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Accept"] = "application/json"
        request.allHTTPHeaderFields = headers
        
        let postString = getPostString(params: paramsBag as! [String : Any])
        request.httpBody = postString.data(using: .utf8)
        
//        This will be use when post data to server using raw json values
//        let data = try! JSONSerialization.data(withJSONObject: paramsBag, options: JSONSerialization.WritingOptions.prettyPrinted)
//        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
//        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        
//        this is use to post data to server using form-data
        
        
        let task = getSession().dataTask(with: request) { (data, response, responseError) in
            NetworkManager.processResponse(data: data, response: response, responseError: responseError, completion: completion)
        }
        task.resume()
    }
    
    fileprivate class func DeleteGenericRequestWithParams(urlString: String, paramsBag: NSDictionary, completion: @escaping (ApiResponse?) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = AppController.shared.loggedInUser?.token
        request.allHTTPHeaderFields = headers
        // will be JSON encoded
        let data = try! JSONSerialization.data(withJSONObject: paramsBag, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        
        let task = getSession().dataTask(with: request) { (data, response, responseError) in
            NetworkManager.processResponse(data: data, response: response, responseError: responseError, completion: completion)
        }
        task.resume()
    }
    
    fileprivate class func putGenericRequest(urlString: String, paramsBag: NSDictionary, completion: @escaping(ApiResponse?) -> ()) {
        
        putGenericRequest(urlString: urlString, paramsBag: paramsBag, token: AppController.shared.loggedInUser?.token, completion: completion)
    }
    
    fileprivate class func putGenericRequest(urlString: String,paramsBag: NSDictionary, token: String?, completion: @escaping (ApiResponse?) -> ()) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = NetworkManager.HTTPMethod.put.rawValue
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = token
        request.allHTTPHeaderFields = headers
        // will be JSON encoded
        let data = try! JSONSerialization.data(withJSONObject: paramsBag, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        
        let task = getSession().dataTask(with: request) { (data, response, responseError) in
            NetworkManager.processResponse(data: data, response: response, responseError: responseError, completion: completion)
        }
        task.resume()
    }
    
    fileprivate class func deleteGenericRequest(urlString: String, completion: @escaping (ApiResponse?) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = NetworkManager.HTTPMethod.delete.rawValue
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = AppController.shared.loggedInUser?.token
        request.allHTTPHeaderFields = headers
        let task = getSession().dataTask(with: request) { (data, response, responseError) in
            NetworkManager.processResponse(data: data, response: response, responseError: responseError, completion: completion)
        }
        task.resume()
    }
    
    fileprivate class func putDataWithImages(urlString: String, imageParamName: String? = nil, image: UIImage? = nil, images: [UIImage]? = nil, imagesParamName: String? = nil, params: NSDictionary, completion: @escaping (ApiResponse?) -> ()) {
        guard let url = URL(string: urlString) else { return }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "PUT"
        let boundary = "Boundary-\(NSUUID().uuidString)"
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue(AppController.shared.loggedInUser?.token ?? "", forHTTPHeaderField: "Authorization")
        let data = NetworkManager.createBodyWithParameters(parameters: params, filePathKey: imageParamName, image: image, images: images, boundary: boundary)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        // Send a POST request to the URL, with the data we created earlier
        getSession(timeOutInterval: 180.0).uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, responseError in
            NetworkManager.processResponse(data: responseData, response: response, responseError: responseError, completion: completion)
        }).resume()
    }
    
    
    
    
    fileprivate class func dataWithImages(method: NetworkManager.HTTPMethod = .post, urlString: String, imageParamName: String, images: [UIImage]? = nil, params: NSDictionary, completion: @escaping (ApiResponse?) -> ()) {
        guard let url = URL(string: urlString) else { return }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        
        let boundary = "Boundary-\(NSUUID().uuidString)"
        
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue(AppController.shared.loggedInUser?.token ?? "", forHTTPHeaderField: "Authorization")
        
        let data = NetworkManager.createBodyWithParameters(parameters: params, filePathKey: imageParamName, images: images, boundary: boundary)
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        // Send a POST request to the URL, with the data we created earlier
        
        getSession(timeOutInterval: 180.0).uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, responseError in
            NetworkManager.processResponse(data: responseData, response: response, responseError: responseError, completion: completion)
        }).resume()
    }
    
    fileprivate class func dataWithImage(method: NetworkManager.HTTPMethod = .post, urlString: String, imageParamName: String, image: UIImage? = nil, images: [UIImage]? = nil, params: NSDictionary, completion: @escaping (ApiResponse?) -> ()) {
        guard let url = URL(string: urlString) else { return }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        
        let boundary = "Boundary-\(NSUUID().uuidString)"
        
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Accept")
        
        let data = NetworkManager.createBodyWithParameters(parameters: params, filePathKey: imageParamName, image: image, images: images, boundary: boundary)
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        // Send a POST request to the URL, with the data we created earlier
        
        getSession().uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, responseError in
            NetworkManager.processResponse(data: responseData, response: response, responseError: responseError, completion: completion)
        }).resume()
    }
    
    // Helper method for multipart for parameters
    private class func createBodyWithParameters(parameters: NSDictionary?, filePathKey: String?, image: UIImage? = nil, images: [UIImage]? = nil, boundary: String) -> Data {
        var body = Data()
        
        if let parameters = parameters {
            for (key, value) in parameters {
                if let key = key as? String, key == "campaign[location_ids][]", let intArray = value as? [Int] {
                    // TODO:_ We have to update the locations when the api will change
                    intArray.forEach { (locationId) in
                        body.append("--\(boundary)\r\n".data(using: .utf8)!)
                        body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                        body.append("\(locationId)\r\n".data(using: .utf8)!)
                    }
                } else {
                    body.append("--\(boundary)\r\n".data(using: .utf8)!)
                    body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                    body.append("\(value)\r\n".data(using: .utf8)!)
                }
            }
        }
        
        if let image = image, let imageData = image.jpeg(.lowest) {
            let filename = "\(filePathKey ?? "filename").jpg"
            
            let mimetype = "image/jpg"
            
            body.append("--\(boundary)\r\n".data(using: .utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: .utf8)!)
            body.append(imageData)
            body.append("\r\n".data(using: .utf8)!)
        }
        
        if let images = images {
            for image in images {
                if let imageData = image.jpeg(.lowest) {
                    let filename = "\(filePathKey ?? "filename").jpg"
                    let mimetype = "image/jpg"
                    body.append("--\(boundary)\r\n".data(using: .utf8)!)
                    body.append("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
                    body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: .utf8)!)
                    body.append(imageData)
                    body.append("\r\n".data(using: .utf8)!)
                }
            }
        }
        
        body.append("--\(boundary)--\r\n".data(using: .utf8)!)
        
        return body
    }
}

// Api Calls

extension NetworkManager {
    class func signUp(fName: String?,lName: String?, email: String?, password: String?, image: UIImage?, completion: @escaping (_ response: ApiResponse?) -> ()) {
        
//        let imgData = image?.jpeg(.lowest)
//        let base64String = imgData?.base64EncodedString(options: .lineLength64Characters)
        
        let userParams = ["email": email ?? "", "password": password ?? "", "first_name": fName ?? "", "last_name": lName ?? ""] as [String : Any]
        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "signup", paramsBag: userParams as NSDictionary, completion: completion)
    }
    
    class func login(email: String?, password: String?, completion: @escaping (_ response: ApiResponse?) -> ()) {
        
        let userParams = ["email": email ?? "", "password": password ?? ""] as [String : Any]
        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "login", paramsBag: userParams as NSDictionary, completion: completion)
    }
    class func Fetchtopfour(completion: @escaping (_ response: ApiResponse?) -> ()) {
        
        NetworkManager.fetchGenericData(urlString: NetworkManager.BASE_URL + "top_four", completion: completion)
    }
    class func updateProfileStatus(status: String?, completion: @escaping (_ response: ApiResponse?) -> ()) {
        
        let userParams = ["user_id": AppController.shared.loggedInUser?.id ?? "", "profile_status": status ?? ""] as [String : Any]
        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "update_profile_status", paramsBag: userParams as NSDictionary, completion: completion)
    }
    class func followUser(followto: Int?, completion: @escaping (_ response: ApiResponse?) -> ()) {
        
        let userParams = ["follower_id": AppController.shared.loggedInUser?.id ?? "", "follow_to": followto ?? ""] as [String : Any]
        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "follow_user", paramsBag: userParams as NSDictionary, completion: completion)
    }
    class func unfollowUser(followto: Int?, completion: @escaping (_ response: ApiResponse?) -> ()) {
        
        let userParams = ["follower_id": AppController.shared.loggedInUser?.id ?? "", "follow_to": followto ?? ""] as [String : Any]
        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "unfollow", paramsBag: userParams as NSDictionary, completion: completion)
    }
}



extension NetworkManager {
    //Network only
    fileprivate class func getDataFromNetwork(urlRequest : URLRequest, completion: @escaping (ApiResponse?) -> ()) {
        getSession().dataTask(with: urlRequest) { (data, response, responseError) in
            NetworkManager.processResponse(data: data, response: response, responseError: responseError, completion: completion)
        }.resume()
    }
    //CacheOnly
    fileprivate class func getDataFromCache(urlRequest : URLRequest , completion: @escaping (ApiResponse?) -> ()) {
        if let cacheResponse = URLCache.shared.cachedResponse(for: urlRequest) {
            guard let json = (try? JSONSerialization.jsonObject(with: cacheResponse.data, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                completion(ApiResponse(message: NetworkResponse.noCacheData.rawValue))
                return
            }
            completion(ApiResponse(json))
        } else {
            completion(ApiResponse(message: NetworkResponse.noCacheData.rawValue))
            return
        }
    }
    //CacheElseNetwork
    fileprivate class func getDataFromCacheElseNetwork(urlRequest : URLRequest, completion: @escaping (ApiResponse?) -> ()) {
        DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
        
        if URLCache.shared.cachedResponse(for: urlRequest) != nil {
            NetworkManager.getDataFromCache(urlRequest: urlRequest, completion: completion)
        } else {
            NetworkManager.getDataFromNetwork(urlRequest: urlRequest, completion: completion)
        }
    }
    
    //NetworkElseCache
    fileprivate class func getDataFromNetworkElseCache(urlRequest : URLRequest, completion: @escaping (ApiResponse?) -> ()) {
        getSession().dataTask(with: urlRequest) { (data, response, responseError) in
            if responseError == nil && data != nil  {
                NetworkManager.processResponse(data: data, response: response, responseError: responseError, completion: completion)
            } else {
                NetworkManager.getDataFromCache(urlRequest: urlRequest, completion: completion)
            }
        }.resume()
    }
    
    //RevalidatingCacheData
    fileprivate class func revalidateCacheData(urlRequest : URLRequest, completion: @escaping (ApiResponse?) -> ()) {
        DispatchQueue.main.async { UIApplication.shared.isNetworkActivityIndicatorVisible = false }
        
        if let cacheResponse = URLCache.shared.cachedResponse(for: urlRequest) {
            NetworkManager.getDataFromCache(urlRequest: urlRequest) { (response: ApiResponse?) in
                completion(response)
                
                getSession().dataTask(with: urlRequest) { (data, response, responseError) in
                    
                    guard responseError == nil else {
                        completion(ApiResponse(message: responseError?.localizedDescription))
                        return
                    }
                    
                    guard let serverData = data else {
                        completion(ApiResponse(message: NetworkResponse.noData.rawValue))
                        return
                    }
                    
                    guard let serverString = String(data: serverData, encoding: String.Encoding.utf8) else {
                        print("Not containing JSON")
                        return
                    }
                    
                    let cachedData = cacheResponse.data
                    
                    guard let cachedString = String(data: cachedData, encoding: String.Encoding.utf8) else {
                        print("Not containing JSON")
                        return
                    }
                    
                    if serverString != cachedString {
                        guard let serverJson = (try? JSONSerialization.jsonObject(with: serverData, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                            print("No data")
                            completion(ApiResponse(message: NetworkResponse.noData.rawValue))
                            return
                        }
                        completion(ApiResponse(serverJson))
                    }
                    
                }.resume()
            }
        } else {
            NetworkManager.getDataFromNetwork(urlRequest: urlRequest, completion: completion)
        }
    }
}
