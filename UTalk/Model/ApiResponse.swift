//
//  ApiResponse.swift
//  UTalk
//
//  Created by Nabeel on 21/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

/// Generic Model for all api calls

struct ApiResponse {
    var status: Int?
    var message: String?
    var data: ResponseData?
    
    init(_ json: [String: Any]) {
        self.status = json["status"] as? Int
        self.message = json["message"] as? String
        self.data = ResponseData.init(response: json["data"] as Any)
    }
    
    init(message: String? = nil) {
        self.message = message
    }
}

struct ResponseData {
    var dictionary: [String: Any]?
    var array: [[String: Any]]?
    
    init(response: Any) {
        if let dictionary = response as? [String: Any] {
            self.dictionary = dictionary
        }
        if let array = response as? [[String: Any]] {
            self.array = array
        }
    }
}
