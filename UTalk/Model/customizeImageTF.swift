//
//  customizeImageTF.swift
//  UTalk
//
//  Created by apple on 5/7/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
@IBDesignable
class customizeImageTF: UITextField {
    
    @IBInspectable var rightImage: UIImage?{
        didSet{
            updateView()
        }
    }
    
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    var bottomBorder = UIView()
    override func awakeFromNib()
    {
        // Setup Bottom-Border
        self.translatesAutoresizingMaskIntoConstraints = false
        bottomBorder = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        bottomBorder.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)// Set Border-Color
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        addSubview(bottomBorder)
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true // Set Border-Strength
    }
    func updateView(){
        if let image = rightImage{
            rightViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 14, height: 14))
            imageView.image = image
            let view = UIView(frame: CGRect(x: -2, y: 0, width: 25, height: 20))
            view.addSubview(imageView)
            rightView = view
        } else{
            leftViewMode = .never
        }
    }
}
