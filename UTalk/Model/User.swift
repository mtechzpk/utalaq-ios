//
//  User.swift
//  UTalk
//
//  Created by Nabeel on 21/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct User: Codable {
    var id: Int?
    var firstName, lastName, email, image: String?
    var verificationCode, profileStatus: String?
    var token: String?
    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case email, image
        case verificationCode = "verification_code"
        case profileStatus = "profile_status"
    }
    
    
    static func toModel(_ json: [String: Any]?) -> User? {
        guard let json = json else { return nil }
        guard let data = try? JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) else { return nil }
        
        let decoder = JSONDecoder()
//        decoder.keyDecodingStrategy = .convertFromSnakeCase
        guard let model = try? decoder.decode(User.self, from: data) else { return nil }
        return model
    }
    static func tousersModel(_ json: [[String: Any]]?) -> Users? {
            guard let json = json else { return nil }
            guard let data = try? JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) else { return nil }
            
            let decoder = JSONDecoder()
    //        decoder.keyDecodingStrategy = .convertFromSnakeCase
            guard let model = try? decoder.decode(Users.self, from: data) else { return nil }
            return model
        }
}
typealias Users = [User]
//struct Users:Codable {
//    var data:[User]?
//
//        static func toModel(_ json: [[String: Any]]?) -> Users? {
//            guard let json = json else { return nil }
//            guard let data = try? JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) else { return nil }
//
//            let decoder = JSONDecoder()
//    //        decoder.keyDecodingStrategy = .convertFromSnakeCase
//            guard let model = try? decoder.decode(Users.self, from: data) else { return nil }
//            return model
//        }
//}
